module.exports = function(config) {
    config.set({
        frameworks: ['jasmine'],
        plugins: [
            'karma-jasmine',
            'karma-junit-reporter',
            'karma-phantomjs-launcher',
            'karma-chrome-launcher',
            'karma-firefox-launcher',
            'karma-ie-launcher'
        ],
        files: [
            '../src/js/**/*.js'
        ],
        reporters: ['progress', 'junit'],
        junitReporter: {
            outputFile: 'test-results.xml'
        },
        port: 8000,
        autoWatch: true,
        background: true,
        exclude: [
            '../../deploy/**/*.js'
        ],
        colors: true,
        browsers: [
            'PhantomJS'
        ],
        singleRun: true
    });
};
