module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        prefix: 'uolpd_',
        dirs: {
            src: "../src",
            test: "../../../test",
            build: "../deploy",
            arroba: "../deploy/arroba",
            barra: "../deploy/barra",
            tab: "../deploy/tab",
            tab_home_nova: "../deploy/tab_home_nova",
            viewport: "../deploy/viewport",
            glider: "../deploy/glider"
        },
        version: {
            txt: ' © UOLPD AD GENERATOR - Todos os direitos reservados'
        },
        concat: {
            options: {
                banner: '(function(window, document, undefined){\n\/*\n *<%= version.txt %> \n *\/ \n\'use strict\'; \n',
                footer: '\n})(this, document);'
            },
            arroba: {
                src: [
                    '<%= dirs.src %>/js/commons/**/*.js',
                    '<%= dirs.src %>/js/utils/**/*.js',
                    '<%= dirs.src %>/js/banners/arroba/ArrobaVideo.js',
                    '<%= dirs.src %>/js/init/arroba/*.js'
                ],
                dest: '<%= dirs.arroba %>/<%= prefix %>arroba.js'
            },
            barra: {
                src: [
                    '<%= dirs.src %>/js/commons/**/*.js',
                    '<%= dirs.src %>/js/utils/**/*.js',
                    '<%= dirs.src %>/js/banners/barra/Barra.js',
                    '<%= dirs.src %>/js/init/barra/*.js'
                ],
                dest: '<%= dirs.barra %>/<%= prefix %>barra.js'
            },
            tab: {
                src: [
                    '<%= dirs.src %>/js/utils/**/*.js',
                    '<%= dirs.src %>/js/banners/tab/Tab.js',
                    '<%= dirs.src %>/js/commons/**/*.js',
                    '<%= dirs.src %>/js/init/tab/*.js'
                ],
                dest: '<%= dirs.tab %>/<%= prefix %>tab.js'
            },
            tab_home_nova: {
                src: [
                    '<%= dirs.src %>/js/utils/**/*.js',
                    '<%= dirs.src %>/js/commons/**/*.js',
                    '<%= dirs.src %>/js/banners/tab_home_nova/Tab.js',
                    '<%= dirs.src %>/js/init/tab_home_nova/*.js'
                ],
                dest: '<%= dirs.tab_home_nova %>/<%= prefix %>tab_home_nova.js'
            },
            viewport: {
                src: [
                    '<%= dirs.src %>/js/utils/**/*.js',
                    '<%= dirs.src %>/js/commons/**/*.js',
                    '<%= dirs.src %>/js/banners/viewport/Viewport.js',
                    '<%= dirs.src %>/js/init/viewport/*.js'
                ],
                dest: '<%= dirs.viewport %>/<%= prefix %><%= viewport %>.js'
            }
        },
        jshint: {
            all: ['<%= concat.arroba.src %>', '<%= concat.barra.src %>', '<%= concat.tab.src %>']
        },
        karma: {
            dist : {
                configFile : 'karma.conf.js'
            },
            dev : {
                options: {
                    configFile : 'karma.conf.js'
                },
                browsers : [
                    'PhantomJS'
                ]
            },
            devWindows : {
                options: {
                    configFile : 'karma.conf.js'
                },
                browsers : [
                    'PhantomJS',
                    'Chrome',
                    'IE'
                ]
            }
        },
        copy: {
            arroba : {
                src: '<%= concat.arroba.dest %>',
                dest: '<%= dirs.arroba %>/<%= prefix %>arroba.js'
            },
            arroba_swf : {
                expand: true,
                flatten: true,
                filter: 'isFile',
                src: '<%= dirs.src %>/swf/arroba/**',
                dest: '<%= dirs.arroba %>'
            },
            arroba_html : {
                expand: true,
                flatten: true,
                filter: 'isFile',
                src: '<%= dirs.src %>/html/arroba/**',
                dest: '<%= dirs.arroba %>'
            },
            barra : {
                src: '<%= concat.barra.dest %>',
                dest: '<%= dirs.barra %><%= prefix %>barra.js'
            },
            barra_swf : {
                expand: true,
                flatten: true,
                filter: 'isFile',
                src: '<%= dirs.src %>/swf/barra/**',
                dest: '<%= dirs.barra %>'
            },
            barra_html : {
                expand: true,
                flatten: true,
                filter: 'isFile',
                src: '<%= dirs.src %>/html/barra/**',
                dest: '<%= dirs.barra %>'
            },
            tab : {
                src: '<%= concat.tab.dest %>',
                dest: '<%= dirs.tab %>/<%= prefix %>tab.js'
            },
            tab_swf : {
                expand: true,
                flatten: true,
                filter: 'isFile',
                src: '<%= dirs.src %>/swf/tab/**',
                dest: '<%= dirs.tab %>'
            },
            tab_html : {
                expand: true,
                flatten: true,
                filter: 'isFile',
                src: '<%= dirs.src %>/html/tab/**',
                dest: '<%= dirs.tab %>'
            },
            tab_home_nova : {
                src: '<%= concat.tab_home_nova.dest %>',
                dest: '<%= dirs.tab_home_nova %>/<%= prefix %>tab_home_nova.js'
            },
            tab_home_nova_swf : {
                expand: true,
                flatten: true,
                filter: 'isFile',
                src: '<%= dirs.src %>/swf/tab_home_nova/**',
                dest: '<%= dirs.tab_home_nova %>'
            },
            tab_home_nova_html : {
                expand: true,
                flatten: true,
                filter: 'isFile',
                src: '<%= dirs.src %>/html/tab_home_nova/**',
                dest: '<%= dirs.tab_home_nova %>'
            },
            viewport : {
                src: '<%= concat.viewport.dest %>',
                dest: '<%= dirs.viewport %>/<%= prefix %>viewport.js'
            },
            viewport_swf : {
                expand: true,
                flatten: true,
                filter: 'isFile',
                src: '<%= dirs.src %>/swf/viewport/**',
                dest: '<%= dirs.viewport %>'
            },
            viewport_html : {
                expand: true,
                flatten: true,
                filter: 'isFile',
                src: '<%= dirs.src %>/html/viewport/**',
                dest: '<%= dirs.viewport %>'
            },
            glider : {
                src: '<%= concat.glider.dest %>',
                dest: '<%= dirs.glider %>/<%= prefix %>glider.js'
            },
            glider_swf : {
                expand: true,
                flatten: true,
                filter: 'isFile',
                src: '<%= dirs.src %>/swf/glider/**',
                dest: '<%= dirs.glider %>'
            },
            glider_html : {
                expand: true,
                flatten: true,
                filter: 'isFile',
                src: '<%= dirs.src %>/html/glider/**',
                dest: '<%= dirs.glider %>'
            },
        },
        clean: {
            options: { force: true },
            build: {
                src: ['<%= dirs.build %>']
            }
        },
        watch: {
            all: {
                files: [
                    '<%= concat.arroba.src %>'
                ],
                options: {
                    livereload: {
                        port: 8000
                    }
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-html-build');
    grunt.loadNpmTasks('grunt-karma');
    grunt.loadNpmTasks('grunt-devperf');
    grunt.loadNpmTasks('grunt-exec');

    grunt.registerTask('build', 'Generate banners', function(format, dev) {
        
        if(arguments.length === 0) {
            grunt.log.writeln('no args');
        } else {
            grunt.log.writeln("Task name: "+this.name);    
            
            if (!format) {
                grunt.log.writeln("Input a banner format");
                return;
            } else {
                grunt.log.writeln("Banner format: "+format);
            }

            if (dev) {
                grunt.log.writeln("Mode: "+dev);    
            }
        }
        
        grunt.task.run('default');
        grunt.task.run(format);
        
        if(dev === "dev") {
            grunt.task.run('dev');
        }

    });

    grunt.registerTask('default',           ['jshint']);
    grunt.registerTask('arroba',            ['copy:arroba','copy:arroba_swf','copy:arroba_html', 'concat:arroba']);
    grunt.registerTask('barra',             ['copy:barra','copy:barra_swf','copy:barra_html', 'concat:barra']);
    grunt.registerTask('tab',               ['copy:tab', 'copy:tab_swf', 'copy:tab_html', 'concat:tab']);
    grunt.registerTask('tab_home_nova',     ['copy:tab_home_nova', 'copy:tab_home_nova_swf', 'copy:tab_home_nova_html', 'concat:tab_home_nova']);
    grunt.registerTask('viewport',          ['copy:viewport', 'copy:viewport_swf', 'copy:viewport_html', 'concat:viewport']);
    grunt.registerTask('glider',          ['copy:glider', 'copy:glider_swf', 'copy:glider_html', 'concat:glider']);
    grunt.registerTask('dev',               ['watch:all']);

};