(function(window, document, undefined){
/*
 * © UOLPD AD GENERATOR - Todos os direitos reservados 
 */ 
'use strict'; 
var UOLPD = UOLPD || {};
window.UOLPD = UOLPD;
function Utils () {

	var $protected = this;
	
	$protected.isMSIE = function() {
		if (navigator.appName === "Microsoft Internet Explorer") {
			return (navigator.userAgent.indexOf("MSIE") != -1);
		} else if(navigator.appName === "Netscape") {
			return (navigator.userAgent.match(/rv:11.0/));
		}
	};

	$protected.checkFlash = function (v) {
		var y, x, s = "Shockwave",
		f = "Flash",
		o = "object",
		u = "undefined",
		np = navigator.plugins,
		nm = navigator.mimeTypes,
		nmd = "application/x-shockwave-flash";
		v = Math.max(Math.floor(v) || 0, 6);
		if (typeof np != u && typeof np[s + " " + f] == o && (x = np[s + " " + f].description) && !(typeof nm != u && nm[nmd] && !nm[nmd].enabledPlugin)) {
			if (v <= x.match(/Shockwave Flash (\d+)/)[1]) return true;
		} else if (typeof window.ActiveXObject != u) {
			for (y = 16; y >= v; y--) {
				try {
					x = new ActiveXObject(s + f + "." + s + f + "." + y);
					if ((x !== null) && (typeof x === o)) return true;
				} catch (e) {}
			}
		}
		return false;
	};

	$protected.createFashvarsEncode = function (flashvars) {
		var str = [];
		for (var vars in flashvars) {
			str.push(encodeURIComponent(vars)+"="+encodeURIComponent(flashvars[vars]));
		} 
		return str.join("&");
	};

}
function ArrobaVideo (debug, tracker) {

	var $protected = this;
	$protected.DEBUG_MODE = debug;
	$protected.ENABLED_TRACKER = tracker;
	$protected.isResumed = false;
	$protected.SWF_W = 300;
	$protected.SWF_H = 250;
	$protected.urlTrackingSmart = "http://www5.smartadserver.com/call/pubimppixel/";
	$protected.urlTrackingUOLMais = "http://mais.uol.com.br/notifyMediaView?t=v&v=2&mediaId=";
	$protected.clickTag = "http://adclick.g.doubleclick.net/aclk?sa=l&ai=CCZiMGNdKVO3DMMvW0AGF8YCgBQEAexABILlgKASIAQGQAQDAAQLIAQngAgGoAwGqBFZP0PwGp1O5QYcSU-pHwrp3LMNJsuGyWQTigkVoLSVkus-lJw4BnBSFBIO0IandOiExN-HhHKzl6S4ksNemYFzKZTvQhXHtyVyOiByOenk_EshkuIO8OLgEAQ&preview=&num=1&sig=AOD64_1bBOt0p-iEmKqpl48tfoa_JmiJQQ&client=ca-pub-1027028252617386&adurl=http://www.hb20.com.br/?utm_source=UOL_Media_WCPackage&utm_medium=Banner_Fullday&utm_content=Retail_Depoimentos_2014_I&utm_campaign=Banner_Fullday_Retail_Depoimentos_2014";
	$protected.imgSrc = "backup.jpg";
	
	$protected.customClientTracker = function(params) {
		var url, path, id, timestamp;
		timestamp = new Date();
		timestamp = timestamp.setDate(timestamp.getDate());
		path = $protected.urlTrackingSmart;

		if ($protected.ENABLED_TRACKER && $protected.DEBUG_MODE) {
			path = "[INSERCAO - SMART_TRACKER_DEBUG_MODE] http://url_smartserver/";
		}

		if (params === undefined) {
			return;
		}

		switch (params) {
			case "close_banner":
				id = "CHANGE_ID";
				$protected.resetResumed();
				break;
			case "expand_banner":
				id = "CHANGE_ID";
				break;
			case "play_video":
				id = "CHANGE_ID";
				$protected.lastStatus = "play_video";
				break;
			case "pause_video":
				id = "CHANGE_ID";
				$protected.resetResumed();
				break;
			case "25":
				id = "CHANGE_ID";
				$protected.resetResumed();
				break;
			case "50":
				id = "CHANGE_ID";
				$protected.resetResumed();
				break;
			case "75":
				id = "CHANGE_ID";
				$protected.resetResumed();
				break;
			case "100":
				id = "CHANGE_ID";
				$protected.resetResumed();
				break;
			case "mute_video":
				id = "CHANGE_ID";
				break;
			case "unmute_video":
				id = "CHANGE_ID";
				break;
			case "resume_video":
				id = "CHANGE_ID";
				$protected.lastStatus = "resume_video";
				break;
			case "buffer_video":
				break;
		}

		if (id === "" || id === undefined) {
			return;
		}

		if($protected.lastStatus === "resume_video") {
			if($protected.isResumed === true)  {
				$protected.lastStatus = "";
				return;			
			}
			$protected.isResumed = true;
		}

		if ($protected.lastStatus === "play_video") {
			$protected.trackingUOLMais();
			$protected.lastStatus = "";
		}

		url = path + id + "/" + timestamp + "?" + params;

		$protected.createTracker(url);

	};

	$protected.trackingUOLMais = function() {
		var url, id, path;

		id = "";
		path = $protected.urlTrackingUOLMais; 

		if ($protected.ENABLED_TRACKER && $protected.DEBUG_MODE) {
			path = "[INSERCAO - UOLMAIS_DEBUG_MODE] http://url_uolmais/";
		}

		url = path + id;

		$protected.createTracker(url);
	};

	$protected.resetResumed = function() {
		$protected.isResumed = false;
		$protected.lastStatus = "";
	};

	$protected.createTracker = function(url) {
		var imgTracker = new Image();
		if ($protected.ENABLED_TRACKER && $protected.DEBUG_MODE) {
			console.log(url);
		} else if ($protected.ENABLED_TRACKER && !$protected.DEBUG_MODE) {
			imgTracker.src = url;
		}
	};

	$protected.isMSIE = function() {
		if (navigator.appName === "Microsoft Internet Explorer") {
			return (navigator.userAgent.indexOf("MSIE") != -1);
		} else if(navigator.appName === "Netscape") {
			return (navigator.userAgent.match(/rv:11.0/));
		}
	};

	$protected.checkFlash = function (v) {
		var y, x, s = "Shockwave",
		f = "Flash",
		o = "object",
		u = "undefined",
		np = navigator.plugins,
		nm = navigator.mimeTypes,
		nmd = "application/x-shockwave-flash";
		v = Math.max(Math.floor(v) || 0, 6);
		if (typeof np != u && typeof np[s + " " + f] == o && (x = np[s + " " + f].description) && !(typeof nm != u && nm[nmd] && !nm[nmd].enabledPlugin)) {
			if (v <= x.match(/Shockwave Flash (\d+)/)[1]) return true;
		} else if (typeof window.ActiveXObject != u) {
			for (y = 16; y >= v; y--) {
				try {
					x = new ActiveXObject(s + f + "." + s + f + "." + y);
					if ((x !== null) && (typeof x === o)) return true;
				} catch (e) {}
			}
		}
		return false;
	};

	$protected.createContainer = function (container) {
		var name = "arroba_video_"+Math.round(Math.random()*99999);
		var div, obj, hasError, params;
		div= document.createElement("div");
		div.id = name;
		params = $protected.params;

		if(!params.config) {
			console.log('Nenhuma configuração foi adicionada ao script');
			hasError = true;
		}

		if(!params.config.movie){
			console.log('Nenhum nome swf foi adicionada as configurações');
			hasError = true;
		}

		if(!$protected.clickTag){
			console.log('Nenhuma clickTag foi configurada');
			hasError = true;
		}

		if(!$protected.imgSrc){
			console.log('Nenhuma imagem foi configurada para a ausência do Flash Player plugin');
			hasError = true;
		}

		if (hasError) {
			return;
		}
	
		if ($protected.checkFlash(10)) {
			obj = $protected.createTagObject(params.config.movie);
		} else {
			obj = $protected.createTagImg($protected.clickTag, $protected.imgSrc);
		}
		
		div.innerHTML = obj.outerHTML;
		
		var divContainer = document.getElementById(container);
		divContainer.appendChild(div);
	};

	$protected.createTagObject = function (name) {
		var value, embed, flashvars, params;
		params = $protected.params;
		
		var obj = document.createElement("object");
		obj.setAttribute("id", "arroba_video");
		
		if ($protected.isMSIE()) {
			obj.setAttribute("classid", "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000");
		} else {
			obj.setAttribute("type","application/x-shockwave-flash");
			obj.setAttribute("data", name);
		}
		
		obj.setAttribute("style", "visibility: visible;");
		obj.setAttribute("width", $protected.SWF_W);
		obj.setAttribute("height", $protected.SWF_H);
		obj.setAttribute("align","middle");
		obj.innerHTML = $protected.createTagParam(params);

		embed = $protected.createTagEmbed(params.config.movie, $protected.createFashvarsEncode(params.config.flashvars));
		obj.appendChild(embed);
		
		return obj;
	};

	$protected.createFashvarsEncode = function (flashvars) {
		var str = [];
		for (var vars in flashvars) {
			str.push(encodeURIComponent(vars)+"="+encodeURIComponent(flashvars[vars]));
		} 
		return str.join("&");
	};

	$protected.createTagEmbed = function (name, flashvars) {
		var obj = document.createElement("embed");
		obj.setAttribute("src", name);
		obj.setAttribute("flashVars", flashvars);
		obj.setAttribute("quality","high");
		obj.setAttribute("type","application/x-shockwave-flash");
		obj.setAttribute("pluginspage","http://www.macromedia.com/go/getflashplayer");
		obj.setAttribute("allowScriptAccess","always");
		obj.setAttribute("width", $protected.SWF_W);
		obj.setAttribute("height" , $protected.SWF_H);
		obj.setAttribute("wmode",'transparent');
		return obj;
	};

	$protected.getBrowser = function() {
		var appName = navigator.appName,
		userAgent = navigator.userAgent,
		n;
		
		var match = userAgent.match(/(opera|chrome|safari|firefox|msie|trident)\/?\s*(\.?\d+(\.\d+)*)/i);

		if (match && (n = userAgent.match(/version\/([\.\d]+)/i)) !== null) {
			match[2] = n[1];
		}
		
		if (match) {
			match =	[match[1], match[2]];
		} else {
			match = [appName, navigator.appVersion, "-?"];
		}

		return match;
	};

	$protected.createTagImg = function (clickTag, path) {
		var link, img;
		link = document.createElement("a");
		link.setAttribute("href", clickTag);
		link.setAttribute("target","_blank");
		img = document.createElement("img");
		img.setAttribute("src", path);
		img.setAttribute("border", 0);
		link.appendChild(img);
		return link;
	};

	$protected.createTagParam = function (params) {
		var param = "";
		for(var name in params.config){
			var value = params.config[name];
			if(name === "flashvars") {
				var flashvars = $protected.createFashvarsEncode(params.config[name]);
				value = flashvars;
			}
			param += "<param name=\"" + name + "\" value=\"" + value + "\" />";
		}
		return param;
	};
}
(function (window, document, undefined) {

	var config = {
		'config': {
			'movie': 'swf_ad1.swf',
			'flashvars': {
				'clickTag': $protected.clickTag,
				'color': '#FFFFFF'
			},
			'quality': 'high',
			'allowScriptAccess': 'always',
			'allowFullScreen':'false',
			'wmode': 'transparent',
			'menu':'false',
			'play':'true'
		},
		'tracking': {
			'start': '1321321564',
			'first_quartle': '1321321564',
			'middle_quartle': '1321321564',
			'complete': '1321321564'
		}
	};

	

	UOLPD.ARROBA_VIDEO = UOLPD.ARROBA_VIDEO || {};
	UOLPD.ARROBA_VIDEO = new ArrobaVideo("banner-300x250-5", config, true, true);
	
})(UOLPD, document);

	
})(this, document);