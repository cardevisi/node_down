(function(window, document, undefined){
/*
 * © UOLPD AD GENERATOR - Todos os direitos reservados 
 */ 
'use strict'; 
function Utils () {

	var $protected = this;
	
	$protected.isMSIE = function() {
		if (navigator.appName === "Microsoft Internet Explorer") {
			return (navigator.userAgent.indexOf("MSIE") != -1);
		} else if(navigator.appName === "Netscape") {
			return (navigator.userAgent.match(/rv:11.0/));
		}
	};

	$protected.checkFlash = function (v) {
		var y, x, s = "Shockwave",
		f = "Flash",
		o = "object",
		u = "undefined",
		np = navigator.plugins,
		nm = navigator.mimeTypes,
		nmd = "application/x-shockwave-flash";
		v = Math.max(Math.floor(v) || 0, 6);
		if (typeof np != u && typeof np[s + " " + f] == o && (x = np[s + " " + f].description) && !(typeof nm != u && nm[nmd] && !nm[nmd].enabledPlugin)) {
			if (v <= x.match(/Shockwave Flash (\d+)/)[1]) return true;
		} else if (typeof window.ActiveXObject != u) {
			for (y = 16; y >= v; y--) {
				try {
					x = new ActiveXObject(s + f + "." + s + f + "." + y);
					if ((x !== null) && (typeof x === o)) return true;
				} catch (e) {}
			}
		}
		return false;
	};

	$protected.createFashvarsEncode = function (flashvars) {
		var str = [];
		for (var vars in flashvars) {
			str.push(encodeURIComponent(vars)+"="+encodeURIComponent(flashvars[vars]));
		} 
		return str.join("&");
	};

}
var UOLPD = UOLPD || {};
window.UOLPD = UOLPD;



function Viewport() {
	var flashvars = {};
	var params = { scale: "exactFit" };
	var attributes = {};
	swfobject.embedSWF("flash1.swf", "flashcontent", "100%", "100%", "7", false, flashvars, params, attributes);
}

var init = new Viewport();

})(this, document);