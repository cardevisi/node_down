(function(window, document, undefined){
/*
 * © UOLPD AD GENERATOR - Todos os direitos reservados 
 */ 
'use strict'; 
function Utils () {
	var $protected = this;
	
	$protected.isMSIE = function() {
		if (navigator.appName === "Microsoft Internet Explorer") {
			return (navigator.userAgent.indexOf("MSIE") != -1);
		} else if(navigator.appName === "Netscape") {
			return (navigator.userAgent.match(/rv:11.0/));
		}
	};

	$protected.checkFlash = function (v) {
		var y, x, s = "Shockwave",
		f = "Flash",
		o = "object",
		u = "undefined",
		np = navigator.plugins,
		nm = navigator.mimeTypes,
		nmd = "application/x-shockwave-flash";
		v = Math.max(Math.floor(v) || 0, 6);
		if (typeof np != u && typeof np[s + " " + f] == o && (x = np[s + " " + f].description) && !(typeof nm != u && nm[nmd] && !nm[nmd].enabledPlugin)) {
			if (v <= x.match(/Shockwave Flash (\d+)/)[1]) return true;
		} else if (typeof window.ActiveXObject != u) {
			for (y = 16; y >= v; y--) {
				try {
					x = new ActiveXObject(s + f + "." + s + f + "." + y);
					if ((x !== null) && (typeof x === o)) return true;
				} catch (e) {}
			}
		}
		return false;
	};

	$protected.createFashvarsEncode = function (flashvars) {
		var str = [];
		for (var vars in flashvars) {
			str.push(encodeURIComponent(vars)+"="+encodeURIComponent(flashvars[vars]));
		} 
		return str.join("&");
	};
}
var UOLPD = UOLPD || {};
window.UOLPD = UOLPD;



function Tab (container, config, debugMode, enabledTracker) {

var delay = 1500;	//Tempo em milissegundos para o banner expandir, 1,5 segundos = 1500
var urlTrackingSmart = "http://www5.smartadserver.com/call/pubimppixel/";
var urlTrackingUOLMais = "http://mais.uol.com.br/notifyMediaView?t=v&v=2&mediaId=";
var utils = new Utils();
var rand = Math.round(Math.random()*99999);

	this.isResumed = false;
	this.DEBUG_MODE = (debugMode) ? debugMode : false;
	this.ENABLED_TRACKER = (enabledTracker) ? enabledTracker : false;
	this.config = config;

	this.customClientTracker = function(config) {
		var url, path, id, timestamp;
		timestamp = (new Date()).getTime();
		path = urlTrackingSmart;

		if (this.ENABLED_TRACKER && this.DEBUG_MODE) {
			path = '[INSERCAO - SMART_TRACKER_DEBUG_MODE] http://url_smartserver/';
		}

		if (config === undefined) {
			return;
		}

		this.resetResumed();

		switch (config) {
			case "close_banner":
				id = "00000000";
				break;
			case "expand_banner":
				id = "00000000";
				break;
			case "play_video":
				id = "00000000";
				this.lastStatus = "play_video";
				break;
			case "pause_video":
				id = "00000000";
				break;
			case "25":
				id = "00000000";
				break;
			case "50":
				id = "00000000";
				break;
			case "75":
				id = "00000000";
				break;
			case "100":
				id = "00000000";
				break;
			case "mute_video":
				id = "00000000";
				break;
			case "unmute_video":
				id = "00000000";
				break;
			case "resume_video":
				id = "00000000";
				this.lastStatus = "resume_video";
				break;
			case "buffer_video":
				return;
		}

		if(this.lastStatus === "resume_video") {
			if(this.isResumed === true)  {
				this.lastStatus = "";
				return;			
			}
			this.isResumed = true;
		}

		if (this.lastStatus === "play_video") {
			this.trackingUOLMais();
		}

		url = path + id + "/" + timestamp + "?" + config;

		this.createTracker(url);

	};

	this.trackingUOLMais = function() {
		var url, id, path;

		id = "";
		path = urlTrackingUOLMais; 

		if (this.ENABLED_TRACKER && this.DEBUG_MODE) {
			path = "[INSERCAO - UOLMAIS_DEBUG_MODE] http://url_uolmais/";
		}

		url = path + id;

		this.createTracker(url);
	};

	this.resetResumed = function() {
		this.isResumed = false;
	};
		
	this.createTracker = function(url) {
		var imgTracker = new Image();
		if (this.ENABLED_TRACKER && this.DEBUG_MODE) {
			console.log(url);
		} else if (this.ENABLED_TRACKER && !this.DEBUG_MODE) {
			imgTracker.src = url;
		}
	};

	this.DEfindPos = function(id) {
		var div = document.getElementById(id);
		var rect = div.getBoundingClientRect();
		return [rect.left, rect.top];
	};

	this.DEgetScroll = function() {
		if (window.pageYOffset !== undefined) {
			return [pageXOffset, pageYOffset];
		} else {
			var sx, sy, d = document, r = d.documentElement, b = d.body;
			sx = r.scrollLeft || b.scrollLeft || 0;
			sy = r.scrollTop || b.scrollTop || 0;
			return [sx, sy];
		}
	};

	this.checkFlash = function (v) {
		var y, x, s = "Shockwave",
			f = "Flash",
			o = "object",
			u = "undefined",
			np = navigator.plugins,
			nm = navigator.mimeTypes,
			nmd = "application/x-shockwave-flash";
		v = Math.max(Math.floor(v) || 0, 6);
		if (typeof np != u && typeof np[s + " " + f] == o && (x = np[s + " " + f].description) && !(typeof nm != u && nm[nmd] && !nm[nmd].enabledPlugin)) {
			if (v <= x.match(/Shockwave Flash (\d+)/)[1]) return true;
		} else if (typeof window.ActiveXObject != u) {
			for (y = 16; y >= v; y--) {
				try {
					x = new ActiveXObject(s + f + "." + s + f + "." + y);
					if ((x !== null) && (typeof x === o)) return true;
				} catch (e) {}
			}
		}
		return false;
	};

	this.DEobj = function(s) {
		if (document.layers) {
			return document.layers[s];
		} else if (document.all && !document.getElementById) {
			return document.all[s];
		} else {
			return document.getElementById(s);
		}
	};

	this.expandTabBannerOuver = function(s, x, y, h, z) {
		var obj = this.DEobj(s);
		window.timeToExpand = setTimeout(function() {
			obj.style.clip = 'rect(0px ' + x + 'px ' + y + 'px ' + z + 'px)';
			this.customClientTracker("expand_banner");
		}, delay);
	};

	this.expandTabBannerOut = function(s, x, y, h, z) {
		var obj = this.DEobj(s);
		obj.style.clip = 'rect(240px ' + x + 'px 300px ' + z + 'px)';
		clearTimeout(window.delay);
		this.customClientTracker("close_banner");
	};

	this.createTagParam = function (params) {

		if(!params) {
			console.log('[UOLPD - createTagParam] parâmetro params necessário');
			return;
		}

		var param = "";
		for(var name in params){
			var value = params[name];
			if(name === "flashvars") {
				var flashvars = utils.createFashvarsEncode(params[name]);
				value = flashvars;
			}
			param += "<param name=\"" + name + "\" value=\"" + value + "\" />";
		}
		return param;
	};

	this.createTagEmbed = function (params) {

		if(!params) {
			console.log('[UOLPD - createTagEmbed] parâmetro params necessário');
			return;
		}

		var obj = document.createElement("embed");
		obj.setAttribute("type","application/x-shockwave-flash");
		obj.setAttribute("pluginspage","http://www.macromedia.com/go/getflashplayer");
		
		for(var name in params) {
			var value = params[name];

			if (name === "movie") {
				name = "src";
			}

			if(name === "flashvars") {
				var flashvars = utils.createFashvarsEncode(params[name]);
				value = flashvars;
			}

			obj.setAttribute(name, value);
		}
		
		return obj;

	};

	this.createTagObject = function (config) {
		var value, embed, params;

		if(!config) {
			console.log('[UOLPD - createTagObject] parâmetro config necessário');
			return;
		}

		if(!config.ad) {
			console.log('[UOLPD - createTagObject] nó ad necessário');
			return;
		}

		if(!config.ad.params) {
			console.log('[UOLPD - createContainers] Configure o nó params em AD');
			return;
		}

		params = config.ad.params;
		
		var obj = document.createElement("object");

		if (config.ad.name) {
			obj.setAttribute("id", config.ad.name+"_tag_object_"+rand);
		}
		
		if (utils.isMSIE()) {
			obj.setAttribute("classid", "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000");
		} else {
			obj.setAttribute("type","application/x-shockwave-flash");
			obj.setAttribute("data", params.movie);
		}
		
		obj.setAttribute("style", "visibility: visible;");
		obj.setAttribute("width", params.width);
		obj.setAttribute("height", params.height);
		obj.setAttribute("align","middle");
		obj.innerHTML = this.createTagParam(params);

		embed = this.createTagEmbed(params);
		obj.appendChild(embed);
		
		return obj;
	};

	this.createTagImg = function (clickTag, path) {
		var link, img;
		link = document.createElement("a");
		link.setAttribute("href", clickTag);
		link.setAttribute("target","_blank");
		img = document.createElement("img");
		img.setAttribute("src", path);
		img.setAttribute("border", 0);
		link.appendChild(img);
		return link;
	};

	this.createDivClipRect = function (params) {
		if(!params) {
			console.log('[UOLPD - createDivClipRect] argumento params obrigatório');
			return;
		}

		if(typeof(params) !== 'object') {
			console.log('[UOLPD - createDivClipRect] argumento params deve ser um objeto');
			return;
		}

		if(params.name === undefined) {
			params.name = "default";
		}

		var div = document.createElement("div");
		var nameDivRect = params.name+"_clip_rect_"+rand;
		var normalHeight = 60;
		div.setAttribute("id", nameDivRect);
		div.setAttribute('onmouseover', 'UOLPD.TAB_VIDEO.expandTabBannerOuver("' + nameDivRect + '",' + params.width + ',' + params.height + ', false, 0);');
		div.setAttribute('onmouseout', 'UOLPD.TAB_VIDEO.expandTabBannerOut("' + nameDivRect + '",' + params.width + ',' + normalHeight + ', true, 0);');
		div.style.position = "absolute";
		div.style.width = params.width;
		div.style.height = params.height;
		div.style.zIndex = 1000;
		div.style.top = ((parseInt(params.height)-normalHeight)*-1)+"px";
		div.style.left = "0px";
		div.style.clip = "rect(70px "+params.width+"px "+params.height+"px 0px)";
		return div;
	};

	this.createDivContainer = function (params) {
		if(!params) {
			console.log('[UOLPD - createDivContainer] argumento params obrigatório');
			return;
		}

		if(typeof(params) !== 'object') {
			console.log('[UOLPD - createDivContainer] argumento params deve ser um objeto');
			return;
		}

		var div = document.createElement("div");
		
		if (params.name) {
			div.setAttribute("id", params.name+"_container_"+rand);
		}
		div.style.position = "relative";
		div.style.width = params.width;
		div.style.height = params.height;
		div.style.zIndex = 9999999999;
		return div;
	};

	this.validateConfig = function (config) {
		if(!config) {
			console.log('[UOLPD - createContainers] É obrigatório passar os parâmetros');
			return false;
		}

		if(!config.ad) {
			console.log('[UOLPD - createContainers] Nenhuma configuração foi adicionada ao script');
			return false;
		}

		if(!config.ad.params || (typeof(config.ad.params) !== 'object')) {
			console.log('[UOLPD - createContainers] Configure o nó params em AD');
			return false;
		}

		if(!config.ad.params.movie){
			console.log('[UOLPD - createContainers] Nenhum nome swf foi adicionada as configurações');
			return false;
		}

		if(!config.ad.backup.clickTag){
			console.log('[UOLPD - createContainers] Nenhuma clickTag foi configurada');
			return false;
		}

		if(!config.ad.backup.image){
			console.log('[UOLPD - createContainers] Nenhuma imagem foi configurada para a ausência do Flash Player plugin');
			return false;
		}

		return true;
	};

	this.createContainers = function (container, config) {
			
		var div, divRect, obj, params;

		if(!this.validateConfig(config)) {
			return;
		}

		params = config.ad.params;

		div = this.createDivContainer(params);
		divRect = this.createDivClipRect(params);

		if (!div && !divRect) {
			return;
		}

		if (this.checkFlash(10)) {
			obj = this.createTagObject(config);
		} else {
			obj = this.createTagImg(config.ad.clickTag, config.ad.image);
		}

		if(!obj){
			console.log('[UOLPD - createContainers] Tag Object ou Img não criada');
			return;
		}

		divRect.innerHTML = obj.outerHTML;
		
		var divContainer = document.getElementById(container);

		if(divContainer === null) {
			console.log('[UOLPD - createContainers] Id do container inexistente');
			return;
		}

		div.appendChild(divRect);
		divContainer.appendChild(div);

	};

	this.init = function () {
		this.createContainers(container, config);
	};
}
var config = {
	'ad': {
		'name': 'banner_tab_home_nova',
		'backup':{
			'image': 'backup.jpg',
			'clickTag': 'http://www.google.com.br'
		},
		'params': {
			'width': 1190,
			'height': 300,
			'movie': '1190x70_expand.swf',
			'quality': 'high',
			'allowScriptAccess': 'always',
			'allowFullScreen':'false',
			'wmode': 'transparent',
			'menu':'false',
			'play':'true',
			'flashvars': {
				'clickTag': 'http://www.google.com.br',
				'color': '#FFFFFF'
			}
		}
	},
	'tracking': {
		'start': '1321321564',
		'first_quartle': '1321321564',
		'middle_quartle': '1321321564',
		'complete': '1321321564'
	}
};

UOLPD.TAB_VIDEO = UOLPD.TAB_VIDEO || {};
UOLPD.TAB_VIDEO = new Tab('banner-1190x70-1', config, true, true);
UOLPD.TAB_VIDEO.init();	
})(this, document);