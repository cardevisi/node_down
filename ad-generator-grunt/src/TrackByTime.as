﻿package {
	
	import CustomClientTracker;
	import DebugTrace;
	
	/**
	 * @author cad_caoliveira
	 */
	 
	public class TrackByTime {
			public static var log:String;
			public static  function videoLogByPoints (currentTime:Number, total:Number, logs:Array, addid:String):void {
				var _total:Number = Math.round(total);
				var percentLogs:Array = logs;
				var percent:Number = 1/(percentLogs.length-1);
				for (var i:int = 0; i <= percentLogs.length-1; i++) {
					var tolerance:Number = (total <= 10) ? 0.5 : 1;
					var min:Number = Math.min((_total*percent*i),(_total*percent*i)-tolerance);
					var max:Number = Math.max((_total*percent*i),(_total*percent*i)+tolerance);
					if(currentTime >= min && currentTime <= max) {
						if(log != percentLogs[i]) {
							log = percentLogs[i];
							DebugTrace.call(addid+".customClientTracker: "+log);
							if(log != "0") {
								CustomClientTracker.call (addid+".customClientTracker ", log);
							}
						}
					} 
					continue;
				}
			}
	}
}
