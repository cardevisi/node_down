﻿package {
	
	/**
	 * @author cad_caoliveira
	 */
		
	public class DebugTrace  {
	
		public static var debugMode:Boolean = false;	
		private static var info:String = "[UOL VIDEO DEBUG_MODE]";
		
		public static function call(message:String):void {
			if (debugMode) {
				trace (info, message);
			}
		}
			
	}
		
}