﻿package {
	
	import flash.external.ExternalInterface;
	/**
	 * @author cad_caoliveira
	 */
		
	
	public class CustomClientTracker  {
		
		private static var msg:String;
		
		public static function call(func:String, value:String):void {
			if (msg != value) {
				if(ExternalInterface.available) {
					ExternalInterface.call(func, value);
				}
				msg = value;
				DebugTrace.call("FLAG VALUE: "+msg);
				DebugTrace.call("CURRENT VALUE: "+value);
			}
		}
	}
		
}