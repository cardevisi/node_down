function Utils () {
	
	var $public = this;
	var $private = {};
	
	$public.isMSIE = function() {
		var appName = navigator.appName;
		var userAgent = navigator.userAgent;
		if (appName === "Microsoft Internet Explorer") {
			return (userAgent.indexOf("MSIE") != -1);
		} else if(appName === "Netscape") {
			return (userAgent.indexOf("rv:11.0") != -1);
		}
	};

	$public.checkFlash = function (version) {
		var index, x, shockWave = "Shockwave",
		flash = "Flash",
		navigatorPlugins = navigator.plugins,
		navigatorMimeTypes = navigator.mimeTypes,
		navigatorMimeTypeFlash = "application/x-shockwave-flash";
		version = Math.max(Math.floor(version) || 0, 6);
		if (typeof navigatorPlugins != 'undefined' && typeof navigatorPlugins[shockWave + " " + flash] == 'object' && (x = navigatorPlugins[shockWave + " " + flash].description) && !(typeof navigatorMimeTypes != 'undefined' && navigatorMimeTypes[navigatorMimeTypeFlash] && !navigatorMimeTypes[navigatorMimeTypeFlash].enabledPlugin)) {
			if (version <= x.match(/Shockwave Flash (\d+)/)[1]) {
				return true;
			}
		} else if (typeof window.ActiveXObject != 'undefined') {
			for (index = 16; index >= version; index--) {
				try {
					x = new ActiveXObject(shockWave + flash + "." + shockWave + flash + "." + index);
					if ((x !== null) && (typeof x === 'object')) return true;
				} catch (e) {}
			}
		}
		return false;
	};

	$public.createFashvarsEncode = function (flashvars) {
		var str = [];
		for (var vars in flashvars) {
			str.push(encodeURIComponent(vars)+"="+encodeURIComponent(flashvars[vars]));
		} 
		return str.join("&");
	};
}