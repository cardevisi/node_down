function Tab (container, config, debugMode, enabledTracker) {
	
	var $public = this;
	var $private = {};
	
	$private.isResumed = false;
	$private.DEBUG_MODE = (debugMode) ? debugMode : false;
	$private.ENABLED_TRACKER = (enabledTracker) ? enabledTracker : false;
	$private.config = config;
	$private.delay = 1500;	//Tempo em milissegundos para o banner expandir, 1,5 segundos = 1500
	$private.urlTrackingSmart = "http://www5.smartadserver.com/call/pubimppixel/";
	$private.urlTrackingUOLMais = "http://mais.uol.com.br/notifyMediaView?t=v&v=2&mediaId=";
	$private.utils = new Utils();
	$private.rand = Math.round(Math.random()*99999);

$private.customClientTracker = function(config) {
	var url, path, id, timestamp;
	timestamp = (new Date()).getTime();
	path = $private.urlTrackingSmart;

	if ($private.ENABLED_TRACKER && $private.DEBUG_MODE) {
		path = '[INSERCAO - SMART_TRACKER_DEBUG_MODE] http://url_smartserver/';
	}

	if (config === undefined) {
		return;
	}

	$private.resetResumed();

	switch (config) {
		case "close_banner":
			id = "00000000";
			break;
		case "expand_banner":
			id = "00000000";
			break;
		case "play_video":
			id = "00000000";
			$private.lastStatus = "play_video";
			break;
		case "pause_video":
			id = "00000000";
			break;
		case "25":
			id = "00000000";
			break;
		case "50":
			id = "00000000";
			break;
		case "75":
			id = "00000000";
			break;
		case "100":
			id = "00000000";
			break;
		case "mute_video":
			id = "00000000";
			break;
		case "unmute_video":
			id = "00000000";
			break;
		case "resume_video":
			id = "00000000";
			$private.lastStatus = "resume_video";
			break;
		case "buffer_video":
			return;
	}

	if($private.lastStatus === "resume_video") {
		if($private.isResumed === true)  {
			$private.lastStatus = "";
			return;			
		}
		$private.isResumed = true;
	}

	if ($private.lastStatus === "play_video") {
		$private.trackingUOLMais();
	}

	url = path + id + "/" + timestamp + "?" + config;

	$private.createTracker(url);

};

$private.trackingUOLMais = function() {
	var url, id, path;

	id = "";
	path = $private.urlTrackingUOLMais; 

	if ($private.ENABLED_TRACKER && $private.DEBUG_MODE) {
		path = "[INSERCAO - UOLMAIS_DEBUG_MODE] http://url_uolmais/";
	}

	url = path + id;

	$private.createTracker(url);
};

$private.resetResumed = function() {
	$private.isResumed = false;
};
	
$private.createTracker = function(url) {
	var imgTracker = new Image();
	if ($private.ENABLED_TRACKER && $private.DEBUG_MODE) {
		console.log(url);
	} else if ($private.ENABLED_TRACKER && !$private.DEBUG_MODE) {
		imgTracker.src = url;
	}
};

$private.DEfindPos = function(id) {
	var div = document.getElementById(id);
	var rect = div.getBoundingClientRect();
	return [rect.left, rect.top];
};

$private.DEgetScroll = function() {
	if (window.pageYOffset !== undefined) {
		return [pageXOffset, pageYOffset];
	} else {
		var sx, sy, d = document,
		r = d.documentElement,
			b = d.body;
		sx = r.scrollLeft || b.scrollLeft || 0;
		sy = r.scrollTop || b.scrollTop || 0;
		return [sx, sy];
	}
};

$private.DEobj = function(s) {
	if (document.layers) {
		return document.layers[s];
	} else if (document.all && !document.getElementById) {
		return document.all[s];
	} else {
		return document.getElementById(s);
	}
};

$public.expandTabBannerOuver = function(s, x, y, h, z) {
	var obj = $private.DEobj(s);
	window.timeToExpand = setTimeout(function() {
		obj.style.clip = 'rect(0px ' + x + 'px ' + y + 'px ' + z + 'px)';
		$private.customClientTracker("expand_banner");
	}, $private.delay);
};

$public.expandTabBannerOut = function(s, x, y, h, z) {
	var obj = $private.DEobj(s);
	obj.style.clip = 'rect(240px ' + x + 'px 300px ' + z + 'px)';
	clearTimeout($private.delay);
	$private.customClientTracker("close_banner");
};

$private.createTagParam = function (params) {

	if(!params) {
		console.log('[UOLPD - createTagParam] parâmetro params necessário');
		return;
	}

	var param = "";
	for(var name in params){
		var value = params[name];
		if(name === "flashvars") {
			var flashvars = $private.utils.createFashvarsEncode(params[name]);
			value = flashvars;
		}
		param += "<param name=\"" + name + "\" value=\"" + value + "\" />";
	}
	return param;
};

$private.createTagEmbed = function (params) {

	if(!params) {
		console.log('[UOLPD - createTagEmbed] parâmetro params necessário');
		return;
	}

	var obj = document.createElement("embed");
	obj.setAttribute("type","application/x-shockwave-flash");
	obj.setAttribute("pluginspage","http://www.macromedia.com/go/getflashplayer");
	
	for(var name in params) {
		var value = params[name];

		if (name === "movie") {
			name = "src";
		}

		if(name === "flashvars") {
			var flashvars = $private.utils.createFashvarsEncode(params[name]);
			value = flashvars;
		}

		obj.setAttribute(name, value);
	}
	
	return obj;

};

$private.createTagObject = function (config) {
	var value, embed, params;

	if(!config) {
		console.log('[UOLPD - createTagObject] parâmetro config necessário');
		return;
	}

	if(!config.ad) {
		console.log('[UOLPD - createTagObject] nó ad necessário');
		return;
	}

	if(!config.ad.params) {
		console.log('[UOLPD - createContainers] Configure o nó params em AD');
		return;
	}

	params = config.ad.params;
	
	var obj = document.createElement("object");
	var idName = config.ad.name+"_tag_object_"+$private.rand;

	if (config.ad.name) {
		obj.setAttribute("id", idName);
	}
	
	if ($private.utils.isMSIE()) {
		obj.setAttribute("classid", "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000");
	} else {
		obj.setAttribute("type","application/x-shockwave-flash");
		obj.setAttribute("data", params.movie);
	}
	
	obj.setAttribute("style", "visibility: visible;");
	obj.setAttribute("width", params.width);
	obj.setAttribute("height", params.height);
	obj.setAttribute("align","middle");

	obj.innerHTML = $private.createTagParam(params);
	embed = $private.createTagEmbed(params);
	obj.appendChild(embed);
	
	return obj;
};

$private.createTagImg = function (clickTag, path) {
	var link, img;
	link = document.createElement("a");
	link.setAttribute("href", clickTag);
	link.setAttribute("target","_blank");
	img = document.createElement("img");
	img.setAttribute("src", path);
	img.setAttribute("border", 0);
	link.appendChild(img);
	return link;
};


//var uolad057876133246_fv = "";
//uolad057876133246_fv += ($private.clickTag.indexOf("http") === 0) ? '&clickTag=' + encodeURIComponent($private.clickTag) : '';

/*var TM = TM || {};
TM.ad = TM.ad || {};
DEAdexp = (TM.ad.expble) ? TM.ad.expble : 1;

// ajuste z-index quando há mais de um banner na pagina
var z1_uolad057876133246 = 999999999;
var z2_uolad057876133246 = 1000;
if (728 < 301) {
	z1_uolad057876133246 = 999999990;
	z2_uolad057876133246 = 900;
}*/



/*if (DEAdexp != 0 && $private.checkFlash(8)) {
	document.write('<div id="DEDivExp2_uolad057876133246" style="position:relative; height:' + DEgifH + 'px; width:' + DEgifW + 'px; z-index:' + z1_uolad057876133246 + ';">');
	if ("down/up" == "up/left") {
		DEgifW2 = DEwdth - DEgifW;
		document.write('<div id="DEDivExp1_uolad057876133246" style="position:absolute; height:' + DEswfH + 'px; width:' + DEwdth + 'px; top:0px; left:-' + DEgifW2 + 'px; clip:rect(0px ' + DEwdth + 'px ' + DEgifH + 'px ' + DEgifW2 + 'px); z-index:' + z2_uolad057876133246 + ';" onMouseOver=$private.expandTabBannerOuver("DEDivExp1_uolad057876133246",' + DEwdth + ',' + DEswfH + ',false,0) onMouseOut=$private.expandTabBannerOuver("DEDivExp1_uolad057876133246",' + DEwdth + ',' + DEgifH + ',true,' + DEgifW2 + ')>');
	} else if ("down/up" == "up/center") {
		DEgifW2 = (DEwdth - DEgifW) / 2;
		DEgifWfinal = DEgifW + DEgifW2;
		document.write('<div id="DEDivExp1_uolad057876133246" style="position:absolute; height:' + DEswfH + 'px; width:' + DEwdth + 'px; top:0px; left:-' + DEgifW2 + 'px; clip:rect(0px ' + DEgifWfinal + 'px ' + DEgifH + 'px ' + DEgifW2 + 'px); z-index:' + z2_uolad057876133246 + ';" onMouseOver=$private.expandTabBannerOuver("DEDivExp1_uolad057876133246",' + DEwdth + ',' + DEswfH + ',false,0) onMouseOut=$private.expandTabBannerOuver("DEDivExp1_uolad057876133246",' + DEgifWfinal + ',' + DEgifH + ',true,' + DEgifW2 + ')>');
	} else if ("down/up" == "down/up") {
		document.write('<div id="DEDivExp1_uolad057876133246" style="position:absolute; height:' + DEswfH + 'px; width:' + DEwdth + 'px; top:-240px; left:0px; clip:rect(240px ' + DEgifW + 'px 300px 0px); z-index:' + z2_uolad057876133246 + ';" onMouseOver=$private.expandTabBannerOuver("DEDivExp1_uolad057876133246",' + DEwdth + ',' + DEswfH + ',false,0) onMouseOut=$private.expandTabBannerOut("DEDivExp1_uolad057876133246",' + DEgifW + ',' + DEgifH + ',true,0)>');
	} else {
		document.write('<div id="DEDivExp1_uolad057876133246" style="position:absolute; height:' + DEswfH + 'px; width:' + DEwdth + 'px; top:0px; left:0px; clip:rect(0px ' + DEgifW + 'px ' + DEgifH + 'px 0px); z-index:' + z2_uolad057876133246 + ';" onMouseOver=$private.expandTabBannerOuver("DEDivExp1_uolad057876133246",' + DEwdth + ',' + DEswfH + ',false,0) onMouseOut=$private.expandTabBannerOuver("DEDivExp1_uolad057876133246",' + DEgifW + ',' + DEgifH + ',true,0)>');
	}
	document.write('<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0" width="' + DEwdth + '" height="' + DEswfH + '"><param name="movie" value="'+SWF_FILE+'"><PARAM NAME="flashvars" VALUE="' + uolad057876133246_fv + '"><param name="quality" value="high"><param name="wmode" value="' + DEwmode + '"><PARAM NAME="allowScriptAccess" VALUE="always"><embed src="'+SWF_FILE+'" flashvars="' + uolad057876133246_fv + '" quality="high" allowScriptAccess="always" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="' + DEwdth + '" height="' + DEswfH + '" WMODE="' + DEwmode + '"></embed></object>');
	document.write('</div></div>');
} else {
	document.write('<a href="http://adclick.g.doubleclick.net/aclk?sa=l&ai=CCZiMGNdKVO3DMMvW0AGF8YCgBQEAexABILlgKASIAQGQAQDAAQLIAQngAgGoAwGqBFZP0PwGp1O5QYcSU-pHwrp3LMNJsuGyWQTigkVoLSVkus-lJw4BnBSFBIO0IandOiExN-HhHKzl6S4ksNemYFzKZTvQhXHtyVyOiByOenk_EshkuIO8OLgEAQ&preview=&num=1&sig=AOD64_1bBOt0p-iEmKqpl48tfoa_JmiJQQ&client=ca-pub-1027028252617386&adurl=http://www.hb20.com.br/?utm_source=UOL_Media_WCPackage&utm_medium=Banner_Fullday&utm_content=Retail_Depoimentos_2014_I&utm_campaign=Banner_Fullday_Retail_Depoimentos_2014" target="_blank"><img src="'+IMG_BACKUP+'" border=0></a>');
}
document.write('<iframe style="display:none;" id="DEprvwIfrm" height="0" width="0" MARGINWIDTH=0 MARGINHEIGHT=0 FRAMEBORDER=0 SCROLLING=no src=""></iframe>');
// v2.0.1
*/
$private.createDivClipRect = function (params) {
	if(!params) {
		console.log('[UOLPD - createDivClipRect] argumento params obrigatório');
		return;
	}

	if(typeof(params) !== 'object') {
		console.log('[UOLPD - createDivClipRect] argumento params deve ser um objeto');
		return;
	}

	var div = document.createElement("div");
	var nameDivRect = params.name+"_clip_rect_"+$private.rand;
	var normalHeight = 60;
	div.setAttribute("id", nameDivRect);
	div.setAttribute('onmouseover', 'UOLPD.TAB_VIDEO.expandTabBannerOuver("' + nameDivRect + '",' + params.width + ',' + params.height + ', false, 0);');
	div.setAttribute('onmouseout', 'UOLPD.TAB_VIDEO.expandTabBannerOut("' + nameDivRect + '",' + params.width + ',' + normalHeight + ', true, 0);');
	div.style.position = "absolute";
	div.style.width = params.width;
	div.style.height = params.height;
	div.style.zIndex = 1000;
	div.style.top = ((parseInt(params.height)-normalHeight)*-1)+"px";
	div.style.left = "0px";
	div.style.clip = "rect(240px 620px 300px 0px)";
	return div;
};

$private.createDivContainer = function (params) {
	if(!params) {
		console.log('[UOLPD - createDivContainer] argumento params obrigatório');
		return;
	}

	if(typeof(params) !== 'object') {
		console.log('[UOLPD - createDivContainer] argumento params deve ser um objeto');
		return;
	}

	var div = document.createElement("div");
	
	if (params.name) {
		div.setAttribute("id", params.name+"_container_"+$private.rand);
	}
	div.style.position = "relative";
	div.style.width = params.width;
	div.style.height = params.height;
	div.style.zIndex = 9999999999;
	return div;
};

$private.createContainers = function (container, config) {
		
		var div, divRect, obj, params;

		if(!config) {
			console.log('[UOLPD - createContainers] É obrigatório passar os parâmetros');
			return;
		}

		if(!config.ad) {
			console.log('[UOLPD - createContainers] Nenhuma configuração foi adicionada ao script');
			return;
		}

		if(!config.ad.params || (typeof(config.ad.params) !== 'object')) {
			console.log('[UOLPD - createContainers] Configure o nó params em AD');
			return;
		}

		if(!config.ad.params.movie){
			console.log('[UOLPD - createContainers] Nenhum nome swf foi adicionada as configurações');
			return;
		}

		if(!config.ad.backup.clickTag){
			console.log('[UOLPD - createContainers] Nenhuma clickTag foi configurada');
			return;
		}

		if(!config.ad.backup.image){
			console.log('[UOLPD - createContainers] Nenhuma imagem foi configurada para a ausência do Flash Player plugin');
			return;
		}

		params = config.ad.params;

		div = $private.createDivContainer(params);
		divRect = $private.createDivClipRect(params);

		if (!div && !divRect) {
			return;
		}

		if ($private.utils.checkFlash(10)) {
			obj = $private.createTagObject(config);
		} else {
			obj = $private.createTagImg(config.ad.clickTag, config.ad.image);
		}

		if(!obj){
			console.log('[UOLPD - createContainers] Tag Object ou Img não criada');
			return;
		}

		divRect.innerHTML = obj.outerHTML;
		
		var divContainer = document.getElementById(container);

		div.appendChild(divRect);
		divContainer.appendChild(div);
	};

	$public.init = function () {
		$private.createContainers(container, config);
	};

}