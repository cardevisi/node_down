var config = {
	'ad': {
		'name': 'banner_tab_home_nova',
		'backup':{
			'image': 'backup.jpg',
			'clickTag': 'http://www.google.com.br'
		},
		'params': {
			'width': 1190,
			'height': 300,
			'movie': '1190x70_expand.swf',
			'quality': 'high',
			'allowScriptAccess': 'always',
			'allowFullScreen':'false',
			'wmode': 'transparent',
			'menu':'false',
			'play':'true',
			'flashvars': {
				'clickTag': 'http://www.google.com.br',
				'color': '#FFFFFF'
			}
		}
	},
	'tracking': {
		'start': '1321321564',
		'first_quartle': '1321321564',
		'middle_quartle': '1321321564',
		'complete': '1321321564'
	}
};

UOLI.GLIDER = UOLI.GLIDER || {};
UOLI.GLIDER = new Glider('banner-1190x70-1', config, true, true);
UOLI.GLIDER.init();	