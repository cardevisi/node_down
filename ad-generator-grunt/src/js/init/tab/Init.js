(function (window, document, undefined) {

	var config = {
		'ad': {
			'name': 'banner_tab',
			'backup':{
				'image': 'backup.jpg',
				'clickTag': 'http://www.google.com.br'
			},
			'params': {
				'width': 620,
				'height': 300,
				'movie': '620x300.swf',
				'quality': 'high',
				'allowScriptAccess': 'always',
				'allowFullScreen':'false',
				'wmode': 'transparent',
				'menu':'false',
				'play':'true',
				'flashvars': {
					'clickTag': 'http://www.google.com.br',
					'color': '#FFFFFF'
				}
			}
		},
		'tracking': {
			'start': '1321321564',
			'first_quartle': '1321321564',
			'middle_quartle': '1321321564',
			'complete': '1321321564'
		}
	};

	window.UOLPD.TAB_VIDEO = window.UOLPD.TAB_VIDEO || {};
	window.UOLPD.TAB_VIDEO = new Tab('banner-620x60-1', config, true, true);
	window.UOLPD.TAB_VIDEO.init();

})(window, document);