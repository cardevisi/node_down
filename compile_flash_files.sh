#!/bin/bash

LIB="/c/CASIFILES/WORKS/adg/trunk/adsGenerator/templates/_lib"
CLASS="${LIB}/br/com/uol/generic/Generic.as"
#muda permissao para 0777
chmod -c a+rwx "$CLASS"
FLASHNAME="flash1"

#**********************************************************************
#for create directories
#**********************************************************************
#mkdir -p 

#**********************************************************************
#Sed is a stream editor - é um utilitário para processamento de texto
#**********************************************************************
sed -i.bak 's/300/728/g' "${CLASS}"
sed -i.bak 's/250/90/g' "${CLASS}"
mxmlc swf-version=11.2 -compiler.source-path="${LIB}" -static-link-runtime-shared-libraries=true "${CLASS}" -output "${FLASHNAME}.swf"
sed -i.bak 's/728/300/g' "${CLASS}"
sed -i.bak 's/90/250/g' "${CLASS}"