var readline = require('readline');
var config = {};

var rl= readline.createInterface({
	input: process.stdin,
	output:process.stdout
});

/*rl.question('Question 1', function(answer) {
	console.log('Answer ' + answer);
	rl.close();
});*/

/*rl.question('Question 2', function(answer) {
	console.log('Answer ' + answer);
	addConfig(answer);
	rl.close();
	rl.write(getConfig());
});*/

rl.on('line', function(line){
    switch(line) {
    	case "passo1":
			addConfig('formato', "200x446");
    		console.log('Digitte o passo 1');
    		break;
    	case "passo2":
			addConfig('name', "casi");
    		console.log('Digitte o passo 2');
    		break;
    	case "close":
			addConfig('close');
    		console.log('Digitte o passo 2');
    		rl.close();
			rl.write(getConfig());
    		break;
    	default:
    		console.log('Digite as opções');
    		break;
    }
})


function addConfig(key, value) {
	config[key] = {};
	config[key] = value;
}

function getConfig() {
	var json = JSON.stringify(config);
	return json;
}
	