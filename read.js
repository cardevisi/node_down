var fs = require('fs');
var inquirer = require("inquirer");
var download_pages = require('download_pages');
var csDown = require('casidown');
var grunt = require('grunt');

//var sys = require('sys');
var exec = require('child_process').exec;
//var toolsv = (process.platform === "win32" ? "tools-v.cmd" : "tools-v");

    //myCmd = "C:\\Users\\win7\\AppData\\Roaming\\npm\\tools-v.cmd";
    //myCmd = toolsv;
    myCmd = 'cd ad-generator-grunt';
    //gui.Shell.openItem('firefox',function(error, stdout, stderr) { });
    //opening Firefox works.
    exec(myCmd,  function (error, stdout, stderr) {
        //detached: true;
        console.log('stdout: ' + stdout);
        
    });

    //exec('ls /c/Users/cad_caoliveira/Desktop/node_down/ad-generator-grunt', function(){ console.log('teste');});

grunt.task.init = function() {};

grunt.initConfig({
  nodewebkit: {
    options: {
        platforms: ['win','osx'],
        buildDir: './webkitbuilds', // Where the build version of my node-webkit app is saved
    },
    src: ['./public/**/*'] // Your node-webkit app
  },
  jshint: {
    all: ['index.js']
  }
});

grunt.loadNpmTasks('grunt-node-webkit-builder');

grunt.registerTask('mytask', function() {
  //grunt.log.write('Ran my task.');
  grunt.task.run("nodewebkit");
});

/*grunt.registerTask('nodewebkit', function() {
  grunt.log.write('Run nodewebkit.');
});*/

// grunt.tasks(['mytask'], {}, function() {
//   grunt.log.ok('Done running tasks.');
// });


var content;
var respostas;
var fileName = "index.html";
var url = "http://dev-uol/intranet/index.html";
var bannersName = ['300x250', '200x446'];
var perguntas = [
	{	
      type: 'input',
      name: 'Nome do cliente:',
      message: 'Digite o nome do cliente ou da campanha a ser veículada:',
      default: 'clienteUOL'
    },
    // {
    //   type: 'input',
    //   name: 'Nome da peça a ser veículada:',
    //   message: '(Exemplo: glider - mosaico - barra - fullscreen)',
    //   default: 'barra'
    // },
    {
      type: 'input',
      name: 'Tipo da peça:',
      message: '(Exemplo: mosaico|glider|barra|cutting)',
      default: 'glider'
    },
    {
      type: 'input',
      name: 'Canal da veiculação:',
      message: '(Exemplo: esporte|economia|jogos|noticias)',
      default: 'home'
    },
    {
      type: 'input',
      name: 'Formato da peça retraida:',
      message: '(Exemplo: 300x250)',
      default: '200x446'
    },
    {
      type: 'input',
      name: 'Formato da peça expandida:',
      message: '(Exemplo: 200x446 - 300x250 - 728x90)',
      default: '1170x900'
    }
];

inquirer.prompt(perguntas, function(respostas){
	//respostas = respostas;
	var name = "", page;
	for(var key in respostas){
		if (key === 'Tipo da peça:') {
			name += respostas[key];
		}
		if (key === 'Nome do cliente:') {
			name += respostas[key];
		}
		if (key === 'Canal da veiculação:') {
			page = respostas[key];
		}
		///console.log('key:', key, 'data:', respostas[key]);
	}	
	getCurrentPage(name, page);
});

function startReadPage (fileName) {
	
	if(fileName === undefined) {
		return;
	}

	fs.readFile(fileName, 'utf-8', function(err, data) {
		if (err) {
			return console.log(err);
		}
		
		//Alteração do path
		if (data.indexOf('%path%') != -1) {
			data = data.replace(/%path%/g, url);
		}

		//console.log('DATA', data);

		//Alteração do nome do arquivo
		if (data.indexOf('%format%') != -1) {
			data = data.replace(/%format%/g, bannersName[0]);
		}
		
		//console.log('match', data.match(/^<div(\s*)?id=\"banner-200x446(-\d*)?\"\>/));
		if (data.match(/^<div(\s*)?id=\"banner-200x446(-\d*)?\"><script(\s*)?type\=\"text\/javascript\"\>TM\.display\(\)\;\<\/script\>\<\/div\>$/)) {
			//data = data.replace(, bannersName[0]);	
		}

		//console.log('match', data.match(/^<div(\s*)?id=\"banner-200x446(-\d*)?\"\>/));
		//if (data.match(/^<div(\s*)?id=\"banner-728x90(-\d*)?\"><script(\s*)?type\=\"text\/javascript\"\>TM\.display\(\)\;\<\/script\>\<\/div\>$/)) {
			//data = data.replace(, bannersName[0]);	
			//console.log("AQUI", data);
		//}

		//console.log("MATH BODY ", data.match(/((<body)(.*)?)(<\/body)/));

		//var first = data.search('<body.*>');
		//var next = data.indexOf('>', first) + 1;

		//console.log("FIRST", first, next);
		//console.log("end", data.substr(next+1) )

		//data = data.substr(0, next+1) +"\n"+"<base href='http://jogos.uol.com.br'\/>";

		//data = data.substr(next+1, data.length);

		//console.log("end", data)

		//"<html><head><title></title></head><body class='st-body' id='container'></body></html>".match(/((<body)(.*)?)(<\/body>)/)[1]+"\n"+"<base href='//jogos.uol.com.br'/>"
		//if (data.match(/((<body)(.*)?)(<\/body>)/)[1]) {
			//data = data[0]+"\n"+"<base href='http://jogos.uol.com.br'\/>";
		//}

		//Transferencia do conteudo de data para a variavel

		var concat = "\n<base href=\"http://jogos.uol.com.br\"/>";
		content = sliceDOM('head', data, concat);
		
		fs.writeFile(fileName, content, function(err){
			if(err) {
				console.log('Erro ao gerar o arquivo');
			}


			console.log('It\'s saved!');
		});

	});
};

/*var fd = fs.open(fileName, 'r+', function(err, fd){
	fs.read(fd, 1000, function(err, bytesRead, buffer){
		console.log(bytesRead);
	});
	//fs.write(fd, "Fala Manu");
});*/

function sliceDOM(tag, data, concat) {
	var tag = 'head'
	var from = data.indexOf('>', data.search('<'+tag+'.*>'))+1;
    var strBody = data.substr(0, from);
    strBody += concat;
    var second = data.substr(from, data.length);
	strBody += second;
	return strBody;
}

function getCurrentPage(projectName, page){
	csDown.init();
	var deployPath = 'deploy';
	var srcPath = 'src';
	var pathFile = projectName+'/'+deployPath+'/index.html';

    makeDirectorys(projectName);
    makeDirectorys(projectName+'/'+srcPath);
    makeDirectorys(projectName+'/'+deployPath);
	
	var url = {'esporte':"http://esporte.uol.com.br", 'home':"http://www.uol.com.br", 'jogos':"http://jogos.uol.com.br"};
	
    console.log(projectName, page, url[page]);

	download_pages.getResponse(url[page], function(data) {
		if (data) {
		    fs.writeFile(pathFile, data, function(err){
		      	if(err) {
		        	return console.log(err);
		      	}
		      	console.log('The files was writed!');
		    });
		    startReadPage(pathFile);
		}
		else console.log("error");  
	});	
};


function makeDirectorys(dir){
	if (!fs.existsSync(dir)){
    	fs.mkdirSync(dir);
	}
};

function copyFile(source, target, cb) {
	var cdCalled = false;

	var rd = fs.createReadStrem(source)
}